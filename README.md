#Project name ---- SHOPBRIDGE
This app is build for admins where they can watch their product list,update list,
create list,delete list and search for list present

##Table of contents
1. [General Info](#general-info)
2. [Technologies](#technologies)
3. [Installation](#installation)

##GeneralInfo
Shopping app build to ease admins job

##Technologies
Ionis framework,Angular framework

##Installation
1.Clone repository from bitbucket
2.Type npm install in terminal from local system folder where repository is saved
3.Run application using (Ionic serve) command 

##

Time for developing the modules 
1)Home page -- 30min
2)Detail page --1 hr
3)Add-edit-item page -- 2hr(with integration)
4)Testing -- 2hr

##screenshots of the apllication

#Home page when app is loaded
![ ](images/Shopbridge1.png)

#When Searched for categories
![ ](images/Shopbridge2.png)

#List of categories inside main category/to add the new item click blue button down
![ ](images/Shopbridge3.png)

#To add a new item



<img src="images/Shopbridge5.png" width=100%>

#After adding a new item
![](images/Shopbridge6.png)

#Editing the item



![ ](images/Shopbridge8.png)

#After editing the item
![ ](images/Shopbridge9.png)

#Deleting the item



![ ](images/Shopbridge10.png)

#After deleting the item
![ ](images/Shopbridge11.png)
