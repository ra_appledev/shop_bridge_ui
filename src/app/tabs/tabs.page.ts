import { Component, ViewChild } from '@angular/core';
import {IonTabs} from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
   @ViewChild(IonTabs, {static : true}) tabs : IonTabs;
   homeSelected;
  constructor() {}

  ngOnInit(){
  }

  ionViewWillEnter(){
    this.homeSelected = (this.tabs.getSelected() === 'home') ? true : false
  }

  tabChanges(e?){
    console.log(e);
    this.homeSelected = (e.tab === 'home') ? true : false
  }

}
