// import { HomePage } from './home/home.page';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http'
import { HttpInterceptorService } from 'src/app/http.interceptor.services';
import {InMemoryWebApiModule} from 'angular-in-memory-web-api'
import { InmemoryService } from 'src/app/services/inmemory.service';


@NgModule({
  declarations: [AppComponent,],
  exports : [],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), 
    AppRoutingModule,HttpClientModule,InMemoryWebApiModule.forRoot(InmemoryService)],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {provide: HTTP_INTERCEPTORS,useClass: HttpInterceptorService, multi: true}
  ],
  bootstrap: [AppComponent],

})
export class AppModule {}
