export interface ICategory {
    id?: number,
    categoryName?: string,
    image?: string,
    createdDate? : Date
  }

  export interface IProduct {
    category?: String,
    id?: number,
    name?: string,
    price?: number,
    image?: string,
    subCategory? : String,
    quantity?: number,
    page? : string,
    data? : {},
    _id? : string,
    categoryName? : string

  }