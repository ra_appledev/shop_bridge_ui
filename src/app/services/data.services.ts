import { Injectable } from '@angular/core';
import { ICategory, IProduct } from '../interfaces/interfaces';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs'
import { catchError } from 'rxjs/operators';

const cudOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

@Injectable({
  providedIn: 'root'
})
export class DataService {
  // realApi_Url = 'https://6f3e49932883.ngrok.io/api/'
  fakeApi_Url = "/api/"
  fakeApi_Title = "/api/categoryTitle"


  constructor(private http: HttpClient) { }

  public getCategoryTitle() {
    return this.http.get(this.fakeApi_Title)
  }

  public getCategories(data?) {
    let category = data
    return this.http.get(this.fakeApi_Url + category)
  }

  public postCategory(data) {
    let data1 = {
      category: data.category
    }
    return this.http.post(`${this.fakeApi_Url + data1.category}`, data)
  }

  public updateCategories(data?) {
    let data1 = {
      category: data.category
    }
    return this.http.put(`${this.fakeApi_Url + data1.category}/` + data.id, data)
  }

  public deleteItem(data?) {
    let data1 = {
      category: data.category
    }
    return this.http.delete(`${this.fakeApi_Url + data1.category}/` + data.id)
  }

  public getSearchResult(data?):Observable<ICategory[]> {
    data = data.trim();
    // add safe, encoded search parameter if term is present
    const options = data ? { params: new HttpParams().set('categoryName', data) } : {};
    return this.http.get<ICategory[]>(this.fakeApi_Title, options).pipe(
      catchError(this.handleError)
    );
  }

  private handleError (error: any) {
    // In a real world app, we might send the error to remote logging infrastructure
    // and reformat for user consumption
    console.error(error); // log to console instead
    return throwError(error);
  }


  //   getCategoryTitle(data?): Observable<ICategory[]>{
  //     return this.http.get<ICategory[]>(`${this.realApi_Url}categoryTitle`);
  //   }

  //   getCategories(data?): Observable<IProduct[]>{
  //     var category = {
  //       category: data
  //   }
  //     return this.http.post<IProduct[]>(`${this.realApi_Url}category/categoryName`,category);
  //   }

  //   updateCategories(id,data?): Observable<IProduct[]>{
  //     return this.http.put<IProduct[]>(`${this.realApi_Url}category/updateSubcategoryItems/`+id,data)
  //   }

  //   postCategory(data?): Observable<IProduct[]>{
  //     return this.http.put<IProduct[]>(`${this.realApi_Url}category/addSubcategory`,data)
  //   }


  //   deleteItem(id,data?): Observable<any>{
  //     return this.http.put<any>(`${this.realApi_Url}category/deleteItem/`+ id ,data)
  //   }

  //   getSearchResult(data?):Observable<ICategory[]>{
  //     let urlSearchParams = new URLSearchParams()
  //     urlSearchParams.append('categoryName' , data)
  //     return this.http.post<ICategory[]>(`${this.realApi_Url}categoryTitle/categoryName?` + urlSearchParams.toString(),null)
  // }

}