import {InMemoryDbService} from 'angular-in-memory-web-api'
import { Injectable } from '@angular/core';
import { ICategory, IProduct } from 'src/app/interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class InmemoryService implements InMemoryDbService {

  constructor() { }

  createDb(){
    let categoryTitle: ICategory[] = [
      {id: 1,categoryName: "Clothes",image: "https://thumbs.dreamstime.com/b/color-gradient-bright-colors-cotton-clothes-rakes-closet-wardrobe-fashion-shop-retail-collection-close-up-142554880.jpg"},
      {id: 2,categoryName: "Electronics",image:"https://image.freepik.com/free-vector/modern-cpu-background-with-gradient-style_23-2147966517.jpg"},
      {id: 3,categoryName: "Groceries",image:"https://thumbs.dreamstime.com/z/food-shopping-delivery-concept-supermarket-basket-groceries-stands-room-door-d-render-green-gradient-199545569.jpg"},
      {id: 4,categoryName: "Sports",image:"https://images.all-free-download.com/images/graphiclarge/competitive_life_background_rushing_action_colorful_silhouette_decor_6827907.jpg"}
    ]

    let Clothes = [
          {id: 1,name: "shirt",image: "https://images.all-free-download.com/images/graphiclarge/competitive_life_background_rushing_action_colorful_silhouette_decor_6827907.jpg",price:"55",quantity:"10"},
          {id: 2,name: "T-shirt",image: "https://images.all-free-download.com/images/graphiclarge/competitive_life_background_rushing_action_colorful_silhouette_decor_6827907.jpg",price:"30",quantity:"11"},
          {id: 3,name: "women t shirt",image: "https://images-na.ssl-images-amazon.com/images/I/613NPOTftaL._UL1500_.jpg",price:"40",quantity:"5"},
          {id: 4,name: "child dress",image: "https://img.joomcdn.net/125784c1a113fdfb421c17a2054c4443678c9e6f_original.jpeg",price:"35",quantity:"12"},
      ]
  

    let Electronics = [
      {id: 1,name: "Mobile",image: "https://image.freepik.com/free-vector/smartphone-gradient-background-mobile-phone-with-abstract-colorful-screen_6343-510.jpg",price:"55",quantity:"10"},
      {id: 2,name: "Commputer",image: "https://image.freepik.com/free-vector/gradient-isometric-laptop-technology-background_52683-6904.jpg",price:"30",quantity:"11"},
      {id: 3,name: "Hp mouse",image: "https://ae01.alicdn.com/kf/HTB1LJmPPXXXXXXlXFXXq6xXFXXXD/100-Original-Steelseries-Rival-300-CSGO-Fade-Edition-Optical-Gradient-Gaming-Mouse-6500CPI-with-retail-box.jpg",price:"40",quantity:"5"},
      {id: 4,name: "Torch lite",image: "https://olightworld.com/image/catalog/Perun%202%20Purple%20Gradient/Perun%202%20purple%20gradient-4.jpg",price:"35",quantity:"12"},
    ]

    let Groceries = [
      {id: 1,name: "Oil",image: "https://media.gettyimages.com/photos/rosemary-oil-rosemary-twig-in-olive-oil-picture-id530073821?s=612x612",price:"55",quantity:"10"},
      {id: 2,name: "Maggi",image: "https://i.pinimg.com/originals/6b/f6/2d/6bf62d635da5f8cfef0d64fdf1440fc4.png",price:"30",quantity:"11"},
      
    ]

    let Sports = [
      {id: 1,name: "Cricket bat",image: "https://d2gg9evh47fn9z.cloudfront.net/800px_COLOURBOX24225939.jpg",price:"55",quantity:"10"},
      {id: 2,name: "kabadi tshirt",image: "https://i.pinimg.com/originals/f3/0a/99/f30a9920202c59badc619c0dd4997578.jpg",price:"30",quantity:"11"},
    
    ]

    return {categoryTitle,Clothes,Electronics,Groceries,Sports};
  
    
  }

}
  


