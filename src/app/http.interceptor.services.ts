import { Injectable } from '@angular/core';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';
import { Observable, pipe } from 'rxjs';
import {tap} from 'rxjs/operators'
import { Data } from '@angular/router';
import { DataService } from 'src/app/services/data.services';


@Injectable()


export class HttpInterceptorService implements HttpInterceptor {
    constructor(private dataService: DataService){}

    intercept(req: HttpRequest<any>, next: HttpHandler):
        Observable<HttpEvent<any>> {
        console.log("interceptor works", req);
        const newReq = req.clone({
            // headers: req.headers.set('Autherization' , currentUser)})
            // setHeaders: {
            //     Authorization: ` ${currentUser ? currentUser : currentToken}`,
            //     Email: `${currentEmail}`
            // }
        })
        return next.handle(newReq)
        .pipe(
            tap((result) => {
                console.log("success",result);
                
            },
            err => {
                console.log("error",err);
            }
            )
        )
    }


}
