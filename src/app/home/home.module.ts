import { HomePage } from './home.page';
import { ItemListComponent } from 'src/app/home/item-list/item-list.component';
import { ItemDetailsPage } from 'src/app/home/item-details/item-details.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { HomePageRoutingModule } from './home-routing.module';
import {DataService} from '../services/data.services'
import { AddEditItemComponent } from 'src/app/home/add-edit-item/add-edit-item.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [ItemDetailsPage,ItemListComponent,AddEditItemComponent,HomePage],
  exports : [ItemListComponent,AddEditItemComponent],
  providers: [DataService]
})
export class HomePageModule {}
