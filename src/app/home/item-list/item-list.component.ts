import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ModalController } from '@ionic/angular';
import { AddEditItemComponent } from 'src/app/home/add-edit-item/add-edit-item.component';
import { IProduct } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.services';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss'],
})
export class ItemListComponent implements OnInit {
  clothes: IProduct[];
  category:any = [];
  data;
  id;
  isLoading = false;
  categoryName;
  search;
  searchBarText = "Search Categories"
  searching;
  item;
  List;
  constructor(private dataService: DataService,
    private router: Router,
    private modalController: ModalController,
    private loadingController: LoadingController) {

  }

  ngOnInit() {
    this.isLoading = true
    this.data = this.item
    this.getCategories(this.data)
  }



  getCategories(data?) {
    this.presentLoading()
    this.dataService.getCategories(data).subscribe((res) => {
      this.isLoading = false;
      this.List = [res]
      this.List.forEach(e => {
      this.category = e
      this.categoryName = e.category
      this.id = e.id;
      console.log(e);
      
      })
      // this.category = res
      // this.id = res['id']
      console.log(this.id);
      this.loaderDismiss()
    })
  }

  addEditItem(page?: string, data?) {
    var item = {
      data: data,
      page: page,
      id: this.id,
      categoryName: this.categoryName
    }
      this.openDescription(item)
  }

  async openDescription(data?) {
    var item = {
      page: data.page,
      id : data.data.id,
      name: data.data.name,
      price: data.data.price,
      quantity: data.data.quantity,
      image: data.data.image,
      categoryName : this.data
    }
    const modal = await this.modalController.create({
      component: AddEditItemComponent,
      cssClass: 'transparent-modal',
      componentProps: {
        item: item ? item : {}

      }
    });
    modal.onDidDismiss().then((res) => {
      // if (res.data) {
        this.getCategories(res.data)
      // }

    })
    return await modal.present();
  }

  close() {
    this.modalController.dismiss();
  }


  async presentLoading() {
    this.isLoading = true;
    return await this.loadingController.create({
      cssClass: 'my-loading-class',
      duration: 2000,
      mode: "md",
      animated: true,
      id: 'todo'
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => 
          console.log('abort presenting')
          )}
      });
    });
  }

  async loaderDismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  


}
