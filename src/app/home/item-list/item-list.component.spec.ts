import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ModalController, AlertController, ToastController } from '@ionic/angular';
import { MockModalController } from 'src/app/mockdata/modal-controller.mock'
import { Router } from '@angular/router';
import { routerMock } from 'src/app/mockdata/router.mock'
import { ADD_EDIT_ITEM_MOCK,LIST_MOCK ,ITEM_LIST_MOCk} from 'src/app/mockdata/add-edit-tem.mock'
import { of } from 'rxjs';
import { DataService } from 'src/app/services/data.services';
import {  FormBuilder } from '@angular/forms';
import { MockToastController, TOAST_MOCK } from 'src/app/mockdata/toast.mock';
import { MockAlertController } from 'src/app/mockdata/alert-controller.mock';
import { ALERT_MOCK } from 'src/app/mockdata/alert.mock';
import { remove } from 'lodash';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { ItemListComponent } from './item-list.component';

describe('ItemListComponent', () => {
  let component: ItemListComponent;
  let fixture: ComponentFixture<ItemListComponent>;
  let dataService: DataService;
  let modalController: MockModalController;
  let httpMock: HttpClient;
  let router: Router;
  let modalCtrlSpy: jasmine.SpyObj<any>;
  let toastCtrlSpy: jasmine.SpyObj<any>
  let originalTimeout;
  let Toast: MockToastController;
  let alertController: MockAlertController

  beforeEach(waitForAsync(() => {

    modalCtrlSpy = jasmine.createSpyObj('ModalController', ['create', 'dismiss'])

    TestBed.configureTestingModule({
      declarations: [ ItemListComponent ],
      imports: [IonicModule.forRoot(),HttpClientTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: ModalController,
          useValue: new MockModalController()
        },
        {
          provide: Router,
          useValue: routerMock
        },
        DataService]
    }).compileComponents();

    fixture = TestBed.createComponent(ItemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemListComponent);
    component = fixture.componentInstance;
    dataService = TestBed.inject(DataService);
    httpMock = TestBed.inject(HttpClient);
    modalController = TestBed.get(ModalController);
    Toast = TestBed.get(ToastController)
    router = TestBed.inject(Router);
    alertController = TestBed.get(AlertController);
    component.ngOnInit();

    fixture.detectChanges();
  });

  afterEach(() => {
    component = null;
    fixture = null;
    dataService = null;
    modalController = null;
    alertController = null;
    router = null;
    httpMock = null;
    modalCtrlSpy = null;
    Toast = null;
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should validate ngOnit',() => {
    spyOn(component , 'getCategories');
    component.ngOnInit()
    expect(component.getCategories).toHaveBeenCalled()
  })


  it('get data from home page' ,() => {
    spyOn(component , 'presentLoading');
    spyOn(component, 'loaderDismiss')
    spyOn(httpMock, 'get').and.returnValue(of(LIST_MOCK.data))
    component.getCategories();
    expect(component.presentLoading).toHaveBeenCalled()
    dataService.getCategories().subscribe((res) => {
      expect(component.isLoading).toBe(false)
      expect(component.List).toEqual(LIST_MOCK.data)
      expect(component.loaderDismiss).toHaveBeenCalled()
    })
  })

  it('should call addedit item ', () => {
    spyOn(component , 'openDescription');
    component.addEditItem();
    expect(component.openDescription).toHaveBeenCalled()
  })

  it('should open addedit modal page', (done: DoneFn) => {
    let item = {page: "edit",id: 1,name: "Ram",price: 10,quantity: 10,image: "dhjfbjd.jpg",categoryName: "clothes"}
    spyOn(httpMock, 'get').and.returnValue(of(LIST_MOCK.data))
    spyOn(modalController, 'onDidDismiss').and.returnValue(Promise.resolve(ITEM_LIST_MOCk));
    component.openDescription();
    modalController.present().then((res) => {
      expect(res).toBeTruthy
      done()
    });
  });

    it('should validate value by clicking back button to hide the modal', (done: DoneFn) => {
      component.close();
      expect(modalCtrlSpy.dismiss).toHaveBeenCalled();
      done();
    });

    it('should dismiss Loader controller', () => {
      component.loaderDismiss();
    })


    
});
