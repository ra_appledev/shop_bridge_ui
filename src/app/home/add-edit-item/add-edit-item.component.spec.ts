import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ModalController, AlertController, ToastController } from '@ionic/angular';
import { MockModalController } from 'src/app/mockdata/modal-controller.mock'
import { Router } from '@angular/router';
import { routerMock } from 'src/app/mockdata/router.mock'
import { ADD_EDIT_ITEM_MOCK } from 'src/app/mockdata/add-edit-tem.mock'
import { of } from 'rxjs';
import { AddEditItemComponent } from './add-edit-item.component';
import { DataService } from 'src/app/services/data.services';
import { FormBuilder } from '@angular/forms';
import { MockToastController, TOAST_MOCK } from 'src/app/mockdata/toast.mock';
import { MockAlertController } from 'src/app/mockdata/alert-controller.mock';
import { ALERT_MOCK } from 'src/app/mockdata/alert.mock';
import { remove } from 'lodash';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';



fdescribe('AddEditItemComponent', () => {
  let component: AddEditItemComponent;
  let fixture: ComponentFixture<AddEditItemComponent>;
  let dataService: DataService;
  let modalController: MockModalController;
  let httpMock: HttpClient;
  let router: Router;
  let modalCtrlSpy: jasmine.SpyObj<any>;
  let toastCtrlSpy: jasmine.SpyObj<any>
  let originalTimeout;
  let Toast: MockToastController;
  let alertController: MockAlertController

  const formBuilder: FormBuilder = new FormBuilder();


  beforeEach(waitForAsync(() => {
    modalCtrlSpy = jasmine.createSpyObj('ModalController', ['create', 'dismiss'])
    toastCtrlSpy = jasmine.createSpyObj('ToastController', ['create', 'present'])


    TestBed.configureTestingModule({
      declarations: [AddEditItemComponent],
      imports: [IonicModule.forRoot(), HttpClientTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],

      providers: [
        {
          provide: ModalController,
          useValue: new MockModalController()
        },
        {
          provide: Router,
          useValue: routerMock
        },
        {
          provide: ToastController,
          useValue: new MockToastController()
        },
        {
          provide: AlertController,
          useValue: new MockAlertController()
        },
        DataService]
    }).compileComponents();

    fixture = TestBed.createComponent(AddEditItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditItemComponent);
    component = fixture.componentInstance;
    dataService = TestBed.inject(DataService);
    httpMock = TestBed.inject(HttpClient);
    modalController = TestBed.get(ModalController);
    Toast = TestBed.get(ToastController)
    router = TestBed.inject(Router);
    alertController = TestBed.get(AlertController);
    component.editForm = formBuilder.group({
      name: '',
      price: '',
      quantity: '',
    });
    component.ngOnInit();

    fixture.detectChanges();
  });

  afterEach(() => {
    component = null;
    fixture = null;
    dataService = null;
    modalController = null;
    alertController = null;
    router = null;
    httpMock = null;
    modalCtrlSpy = null;
    Toast = null;
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('field validity', () => {
    let errors = {};
    let title = component.editForm.controls['name'];
    expect(title.valid).toBeFalsy();

    errors = title.errors || {}
    expect(errors['required']).toBeTruthy();

  });

  it('should have form with expected fields', () => {
    expect(component.editForm.value).toEqual({ name: '', price: '', quantity: '' })
  });

  it('should check for validator error and fail', () => {
    expect(component.editForm.valid).toBeFalsy()
  })

  it('should validate ngOnit', (done: DoneFn) => {
    component.page = "Edit"
    spyOn(component, 'createForm')
    expect(component.page).toEqual("Edit");
    component.item.categoryName = 'Clothes';
    component.ngOnInit();
    expect(component).toBeTruthy()
    expect(component.createForm).toHaveBeenCalled()
    expect(component.categoryName).toBe(component.item.categoryName);
    expect(component.page).toBe(component.item.page);
  })

  it('should create the createForm', (done: DoneFn) => {
    component.createForm()
  })

  it('should save profile', (done: DoneFn) => {
    spyOn(httpMock, 'post').and.returnValue(of(null));
    spyOn(component, 'updateItem')
    spyOn(component, 'saveItem')
    component.saveprofile();
    expect(component.updateItem).toHaveBeenCalled()
    expect(component.saveItem).toHaveBeenCalled()

  })

  it('should save item', () => {
    spyOn(httpMock, 'post').and.returnValue(of(null));
    spyOn(component, 'close');
    spyOn(component, 'presentToast')
    let data = {
      name: "shirt",
      price: 100,
      quantity: 10,
      id: '609b69a23f1fa68c3443e41a',
      category: "609b69a23f1fa68c3443e41a"
    }
    component.saveItem()
    expect(httpMock.post).toHaveBeenCalledTimes(1);
    dataService.postCategory(data).subscribe(res => {
      expect(component.presentToast).toHaveBeenCalled()
      expect(component.close).toHaveBeenCalled();
    })
  })

  it('should update item', () => {
    spyOn(httpMock, 'put').and.returnValue(of(null));
    spyOn(component, 'close');
    spyOn(component, 'presentToast')
    expect(component.page).toBeUndefined()
    let data = {
      name: "T-shirt",
      price: 100,
      quantity: 10,
      id: '609b69a23f1fa68c3443e41a',
      category: "609b69a23f1fa68c3443e41a"
    }
    component.updateItem()
    expect(httpMock.put).toHaveBeenCalledTimes(1);
    dataService.updateCategories(data).subscribe(res => {
      expect(component.presentToast).toHaveBeenCalled()
      expect(component.close).toHaveBeenCalled();
    })
  })

  it('should delete the item', () => {
    spyOn(httpMock, 'delete').and.returnValue(of(null))
    spyOn(component, 'close');
    let data = {
      id: "609b69a23f1fa68c3443e41a"
    }
    component.deleteItem()
    expect(httpMock.delete).toHaveBeenCalledTimes(1);
    dataService.deleteItem(data).subscribe(res => {
      expect(component.close).toHaveBeenCalled()
    })
  })

  it('should confirm and delete journal', fakeAsync((done: DoneFn) => {
    spyOn(httpMock, 'get').and.returnValue(of(ADD_EDIT_ITEM_MOCK));
    spyOn(httpMock, 'delete').and.returnValue(of(null));
    spyOn(component, 'deleteItem')
    ALERT_MOCK.buttons[0].handler = () => void (0);
    ALERT_MOCK.buttons[1]['handler'] = () => {
      remove(component.item, ['id', ADD_EDIT_ITEM_MOCK[1].id]);
      component.deleteItem();
    };
    component.deleteAlert();
    tick();
    expect(alertController.getLast().visible).toBeTruthy();
    expect(alertController.getLast().header).toBe(ALERT_MOCK.header);
    expect(alertController.getLast().message).toBe(ALERT_MOCK.message);
    expect(alertController.getLast().buttons.length).toBe(ALERT_MOCK.buttons.length);
    expect(alertController.getLast().buttons[0].text).toBe(ALERT_MOCK.buttons[0].text);
    expect(alertController.getLast().buttons[1].text).toBe(ALERT_MOCK.buttons[1].text);
    alertController.getLast().buttons[0].handler();
    alertController.getLast().buttons[1].handler();
    alertController.present().then((res) => {
      expect(res).toBe(true);
      expect(component.deleteItem).toHaveBeenCalled()
      expect(component.item.data).toBe(ADD_EDIT_ITEM_MOCK.length);
      expect(httpMock.get).toHaveBeenCalledTimes(1);
    });
  }));

  it('should create ToastController', (done: DoneFn) => {
    component.presentToast()
    expect(Toast.getLast().visible).toBeTruthy();
    expect(Toast.getLast().cssClass).toBe(TOAST_MOCK.cssClass);
    expect(Toast.getLast().message).toBe(TOAST_MOCK.message);
    expect(Toast.getLast().duration).toBe(TOAST_MOCK.duration);
    expect(Toast.getLast().position).toBe(TOAST_MOCK.position);
    Toast.present()
  })

  it('should validate value by clicking back button to hide the modal', (done: DoneFn) => {
    component.close();
    expect(modalCtrlSpy.dismiss).toHaveBeenCalledTimes(1);
    done();
  });

});
