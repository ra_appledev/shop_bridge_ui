import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { IProduct } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.services';

@Component({
  selector: 'app-add-edit-item',
  templateUrl: './add-edit-item.component.html',
  styleUrls: ['./add-edit-item.component.scss'],
  providers: [FormBuilder]
})
export class AddEditItemComponent implements OnInit {
  editForm;
  name;
  price;
  quantity;
  data;
  page;
  subCategoryId;
  categoryId;
  categoryName;
  buttonName = 'Save';
  item: IProduct;
  showDeleteButton: boolean = false;
  image;
  constructor(
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private router: Router,
    private toastController: ToastController,
    private modalController: ModalController,
    private alertController: AlertController,
  ) { }

  ngOnInit() {
    console.log(this.item);

    if (this.item.page == "Edit") {
      this.buttonName = 'Edit';
      this.showDeleteButton = true
    } else {
      this.buttonName = 'Save';
    }
    this.subCategoryId
    this.categoryId = this.item.id
    this.categoryName = this.item.categoryName
    this.page = this.item.page;
    this.createForm()
    this.data = this.item
    this.subCategoryId = this.data.id
    this.name = this.data.name
    this.price = this.data.price;
    this.quantity = this.data.quantity;
    this.image = this.data.image
  }

  createForm() {
    this.editForm = this.formBuilder.group({
      name: [this.name, [Validators.required, Validators.maxLength(50)]],
      price: [this.price],
      quantity: [this.quantity]
    });
  }

  saveprofile() {
    var data = {
      name: this.editForm.value.name,
      price: this.editForm.value.price,
      quantity: this.editForm.value.quantity,
      id: this.subCategoryId,
      category: this.categoryName,
      image : this.item.page == "Edit" ? this.image : 'https://cdn.pixabay.com/photo/2012/04/13/12/30/new-32199_1280.png'
    }
    if (this.page == "Edit") {
      this.updateItem(data)
    } else {
      this.saveItem(data)
    }
  }

  saveItem(data?) {
    this.dataService.postCategory(data).subscribe(res => {
        this.presentToast('Your New Item has been Saved successfully')
        setTimeout(() => {
          this.close()
        }, 2000);
    })
  }

  updateItem(data?) {
    this.dataService.updateCategories(data).subscribe(res => {
        this.presentToast('Your New Item has been Edited successfully')
        setTimeout(() => {
          this.close()
        }, 2000);
    })
  }

  deleteItem() {
    var data = {
      id: this.subCategoryId,
      category : this.categoryName
    }
    this.dataService.deleteItem(data).subscribe(res => {
      this.presentToast('Your New Item has been Deleted successfully')
      setTimeout(() => {
        this.close()
      }, 2000);

    })
  }

  async presentToast(message?) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      cssClass: 'toast-custom1',
      position: 'bottom'
    });
    toast.present();
  }

  close() {
    this.modalController.dismiss(this.categoryName)
  }

  async deleteAlert() {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure you want to delete this Item?',
      cssClass: 'todo-alert',
      buttons: [
        {
          text: 'Yes',
          role: 'yes',
          handler: () => {
            console.log('Confirm  blah');
            this.deleteItem()
          }
        }, {
          text: 'No',
          handler: () => {
            console.log('NO is clicked');
          }
        }
      ]
    });
    await alert.present();
  }

}
