import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { debounceTime } from 'rxjs/operators';
import { ItemListComponent } from 'src/app/home/item-list/item-list.component';
import { DataService } from 'src/app/services/data.services';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  public categories: any = [];
  searchControl: FormControl;
  searchBarText = "Search Categories"
  List;
  searching;
  show;
  searchTitle;
  search;
  constructor(
    private data: DataService,
    private router: Router,
    private modalController: ModalController
  ) {
    this.searchControl = new FormControl();

  }

  ngOnInit() {
    this.searchControl.valueChanges.pipe(debounceTime(700)).subscribe(search => {
      this.search = search
      if (search != '') {
        this.getSearchResult(this.search)
      }
      else {
        this.getCategories()
      }
    })
    this.getCategories()

    console.log(this.searchTitle);

  }

  getSearchResult(data?) {
    this.data.getSearchResult(data).subscribe(res => {
      this.categories = res;
      this.searching = false
      if (!this.categories != null) {
        this.show = false
      } else {
        this.show = true
      }
      console.log(this.show);
    })
  }

  getCategories() {
    this.data.getCategoryTitle().subscribe(res => {
      this.categories = res
      this.searching = false
      if (this.categories != null) {
        this.show = false
      } else {
        this.show = true
      }
      console.log(this.show);
    })
  }

  goToItemDetails(data?) {
    this.openDescription(data)

  }

  onSearchInput(e?) {
    var detail = e.detail.value
    if (detail) {
      this.searching = true
    } else {
      this.searching = false
    }
  }

  async openDescription(data?) {
    const modal = await this.modalController.create({
      component: ItemListComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        item: data ? data : {}

      }
    });
    modal.onDidDismiss().then((res) => {
      if (res.data) {
      }

    })
    return await modal.present();
  }



}
