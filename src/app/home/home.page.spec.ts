import { By } from '@angular/platform-browser';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ModalController } from '@ionic/angular';
import { MockModalController } from 'src/app/mockdata/modal-controller.mock'
import { Router } from '@angular/router';
import { routerMock } from 'src/app/mockdata/router.mock'
import { ADD_EDIT_ITEM_MOCK, ITEM_LIST_MOCk } from 'src/app/mockdata/add-edit-tem.mock'
import { of } from 'rxjs';
import { DataService } from 'src/app/services/data.services';
import { FormControl } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { HomePage } from './home.page';
import { debounceTime } from 'rxjs/operators';

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;
  let dataService: DataService;
  let modalController: MockModalController;
  let httpMock: HttpClient;
  let router: Router;
  let modalCtrlSpy: jasmine.SpyObj<any>;

  beforeEach(waitForAsync(() => {
    modalCtrlSpy = jasmine.createSpyObj('ModalController', ['create', 'dismiss'])

    TestBed.configureTestingModule({
      declarations: [HomePage],
      imports: [IonicModule.forRoot(), HttpClientTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],

      providers: [
        {
          provide: ModalController,
          useValue: new MockModalController()
        },
        {
          provide: Router,
          useValue: routerMock
        },
        DataService]
    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    dataService = TestBed.inject(DataService);
    httpMock = TestBed.inject(HttpClient);
    modalController = TestBed.get(ModalController);
    router = TestBed.inject(Router);
    component.ngOnInit();

    fixture.detectChanges();
  });

  afterEach(() => {
    component = null;
    fixture = null;
    dataService = null;
    modalController = null;
    router = null;
    httpMock = null;
    modalCtrlSpy = null;
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should validate ngonit', (done: DoneFn) => {
    spyOn(component, 'getSearchResult')
    spyOn(component, 'getCategories')
    spyOn(httpMock, 'get').and.returnValue(of(ADD_EDIT_ITEM_MOCK))
    const input = fixture.debugElement.query(By.css('ion-searchbar'))
    input.triggerEventHandler('ionChange', {});
    component.ngOnInit();
    component.searchControl = new FormControl();
    component.searchControl.valueChanges.pipe(debounceTime(700)).subscribe(() => {
      expect(fixture.debugElement.query(By.css('ion-searchbar')).nativeElement.innerText).toEqual("Clothes")
      expect(component.search).toEqual("Clothes");
      expect(component.getCategories).toHaveBeenCalled();
      done();
    })
  });


  it('should get Category list when searched', (done: DoneFn) => {
    spyOn(httpMock, 'get').and.returnValue(of(ADD_EDIT_ITEM_MOCK))
    component.getSearchResult(component.search);
    dataService.getSearchResult(component.search).subscribe(res => {
      expect(res).toEqual(ADD_EDIT_ITEM_MOCK)
      expect(component.categories).toEqual(ADD_EDIT_ITEM_MOCK);
      done();
    });
  });

  it('should get cetagories', () => {
    spyOn(httpMock, 'get').and.returnValue(of(ADD_EDIT_ITEM_MOCK))
    component.getCategories();
    dataService.getCategoryTitle().subscribe(res => {
      expect(component.List != null).toBe(true)
      expect(component.categories).toEqual(ADD_EDIT_ITEM_MOCK)
      expect(component.show).toBeTruthy
    })
  })

  it('should check e.detail.value is not present', (done: DoneFn) => {
    var detail = "clothes"
    const input = fixture.debugElement.query(By.css('ion-searchbar'))
    input.triggerEventHandler('ionChange', {});
    component.onSearchInput();
    expect(fixture.debugElement.query(By.css('ion-searchbar')).nativeElement.innerText).toEqual(detail)
    done();
  });

  it('should gotoItemDetails', () => {
    spyOn(component, 'openDescription')
    component.goToItemDetails()
    expect(component.goToItemDetails).toHaveBeenCalled()
  })

  it('should open modal entry', (done: DoneFn) => {
    spyOn(httpMock, 'get').and.returnValue(of(ADD_EDIT_ITEM_MOCK))
    spyOn(modalController, 'onDidDismiss').and.returnValue(Promise.resolve(ITEM_LIST_MOCk));
    component.openDescription();
    modalController.present().then((res) => {
      expect(res).toBe(true)
      done()
    })
  });
});
