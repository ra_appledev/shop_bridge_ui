import {IProduct} from 'src/app/interfaces/interfaces';

export const ADD_EDIT_ITEM_MOCK : IProduct[] =[
    {
        name: "Mobile",
        price: 100,
        quantity : 10,
        image: 'https://cdn.pixabay.com/photo/2012/04/13/12/30/new-32199_1280.png'
    },
    {
        name: "Computer",
        price: 100,
        quantity : 10,
        image: 'https://cdn.pixabay.com/photo/2012/04/13/12/30/new-32199_1280.png'
    },
   
    
]

export const LIST_MOCK  = {
    data : {
        name: "Mobile",
        price: 100,
        // quantity : 10,
        image: 'https://cdn.pixabay.com/photo/2012/04/13/12/30/new-32199_1280.png'
    },
    role: undefined

}

export const ITEM_LIST_MOCk = {
    data:undefined,
    role: undefined
};

