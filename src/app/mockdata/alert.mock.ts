export const ALERT_MOCK = {
    header: 'Confirm',
    message: 'Are you sure, you want to delete this Journal?',

    buttons: [
        {
            text: 'No',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => void (0)
            // handler: () => {
            //     JournalSliderRef.close();
            // }
        }, {
            text: 'Yes',
            handler: () => void (0)
        }
    ]
};