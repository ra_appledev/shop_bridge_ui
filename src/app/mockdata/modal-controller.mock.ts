import { Observable, Subject } from 'rxjs';

interface DismissModal {
    data: any;
    role: string;
}

const DEFAULT_MODAL_DISMISS: DismissModal = {
    data: undefined,
    role: undefined,
}

export class MockModal {
    public visible: boolean;
    public component: any;
    public componentProps: any;

    constructor(props: any) {
        Object.assign(this, props);
        this.visible = false;
    }

    present() {
        this.visible = true;
        return Promise.resolve();
    }

    onDidDismiss() {
        this.visible = false;
        return Promise.resolve(DEFAULT_MODAL_DISMISS);
    }

}

export class MockModalController {

    public created: MockModal[];
    private props: any;

    constructor() {
        this.created = [];
    }

    create(props: any): Promise<any> {
        this.props = props;
        const modal = new MockModal(props);
        modal.onDidDismiss = () => this.onDidDismiss();
        this.created.push(modal);
        return Promise.resolve(modal);
    }

    getLast() {
        if (!this.created.length) {
            return null;
        }
        return this.created[this.created.length - 1];
    }

    present() {
        return Promise.resolve(true);
    }

    onDidDismiss() {
        return Promise.resolve(DEFAULT_MODAL_DISMISS)
    };
}