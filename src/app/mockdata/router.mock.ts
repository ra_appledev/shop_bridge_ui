import { ReplaySubject } from 'rxjs';
import { RouterEvent } from '@angular/router';
export const routerMockEventSubject = new ReplaySubject<RouterEvent>(1);

export const routerMock = {
    url: 'test/url',
    events: routerMockEventSubject.asObservable(),
    navigate: jasmine.createSpy('navigate')
}