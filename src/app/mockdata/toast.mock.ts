

export class MockToast {
    public duration: Number;
    public cssClass: string;
    public position: string;
    public message: string;
    public visible : boolean

    constructor(props: any) {
        Object.assign(this, props);
        this.visible = false;
    }

    present() {
        this.visible = true;
        return Promise.resolve();
    }

    dismiss() {
        this.visible = false;
        return Promise.resolve();
    }

}

export class MockToastController {

    public created: MockToast[];

    constructor() {
        this.created = [];
    }

    create(props: any): Promise<any> {
        const toRet = new MockToast(props);
        this.created.push(toRet);
        return Promise.resolve(toRet);
    }

    getLast() {
        if (!this.created.length) {
            return null;
        }
        return this.created[this.created.length - 1];
    }

    present() {
        return Promise.resolve(true);
    }

}


export const TOAST_MOCK = {
    message: 'MESSAGE',
    duration: 2000,
    cssClass: 'toast-custom1',
    position: 'bottom'
}