import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AddEditItemComponent } from 'src/app/home/add-edit-item/add-edit-item.component';
import { ItemListComponent } from 'src/app/home/item-list/item-list.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'item-details',
    loadChildren: () => import('./home/item-details/item-details.module').then( m => m.ItemDetailsPageModule)
  },
  {
    path: 'item-list',
    component : ItemListComponent
  },
  {
    path: 'add-edit-item',
    component : AddEditItemComponent
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
